package finalProject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;


public class BusSystem {
	ArrayList<BusStop> stops = new ArrayList<BusStop>();
	ArrayList<Trip> trips = new ArrayList<Trip>();
	ArrayList<StopTime> stopTimes = new ArrayList<StopTime>();
	TST<BusStop> busStopTST = new TST<BusStop>();
	TST<StopTime> stopTimeTST = new TST<StopTime>();
	
	//Constructor
	BusSystem(String stopsFile, String transfersFile, String stopTimesFile)
	{
		try {
			FileReader fr1 = new FileReader(stopsFile);
			BufferedReader br1 = new BufferedReader(fr1);
			String line1 = br1.readLine();
			int stopIDstops;
			int stopCode;
			String stopName;
			String stopDesc;
			Double stopLat;
			Double stopLon;
			String zoneID;
			String stopURL;
			int locationType;
			int parentStation;
			line1 = br1.readLine();
			int value = 0;
			while(line1!=null) 
			{
				String[] lineSplit = line1.trim().split(",");
				/*
				for(int i=0;i<lineSplit.length;i++) 
				{
					System.out.println(lineSplit[i]);
				}
				System.out.print("\n");
				*/
				if(!lineSplit[0].contains(" ")) 
				{
					stopIDstops = Integer.parseInt(lineSplit[0]);
				}
				else 
				{
					stopIDstops = -1;
				}
				if(!lineSplit[1].contains(" "))
				{
					stopCode = Integer.parseInt(lineSplit[1]);
				}
				else 
				{
					stopCode = -1;
				}
				if(!lineSplit[2].equals(" "))
				{
					stopName = lineSplit[2];
				}
				else 
				{
					stopName = null;
				}
				if(!lineSplit[3].equals(" ")) 
				{
					stopDesc = lineSplit[3];
				}
				else 
				{
					stopDesc = null;
				}
				if(!lineSplit[4].contains(" "))
				{
					stopLat = Double.parseDouble(lineSplit[4]);
				}
				else 
				{
					stopLat = -1.0;
				}
				if(!lineSplit[5].contains(" "))
				{
					stopLon = Double.parseDouble(lineSplit[5]);
				}
				else 
				{
					stopLon = -1.0;
				}
				if(!lineSplit[6].equals(" "))
				{
					zoneID = lineSplit[6];
				}
				else 
				{
					zoneID = null;
				}
				if(!lineSplit[7].equals(" "))
				{
					stopURL = lineSplit[7];
				}
				else 
				{
					stopURL = null;

				}
				if(!lineSplit[8].equals(" "))
				{
					locationType = Integer.parseInt(lineSplit[8]);
				}
				else 
				{
					locationType = -1;
				}
				
				if(lineSplit.length<10) 
				{
					stops.add(new BusStop(stopIDstops, stopCode, stopName, stopDesc, stopLat, stopLon, 
							zoneID, stopURL, locationType));
				}
				else 
				{
					if(!lineSplit[9].contains(" "))
					{
						parentStation = Integer.parseInt(lineSplit[9]);
					}
					else 
					{
						parentStation = -1;
					}
					stops.add(new BusStop(stopIDstops, stopCode, stopName, stopDesc, stopLat, stopLon, 
							zoneID, stopURL, locationType, parentStation));
				}
				
				String[] stopNameSplit = null;
				if(stopName!=null) 
				{	
					stopNameSplit = stopName.split("\\s+");
					String stopNameFlagstopSwitched ="";
					int j=1;
					if(stopNameSplit[0].equals("")) 
					{
						j++;
					}
					else 
					{
						//No change
					}
					if((stopNameSplit[j-1].equals("FLAGSTOP"))) 
					{
						if (stopNameSplit[j].length()==2) 
						{
							for(int i=j+1;i<stopNameSplit.length;i++) 
							{
								stopNameFlagstopSwitched+=stopNameSplit[i]+" ";
							}
							stopNameFlagstopSwitched+=stopNameSplit[j-1]+" "+stopNameSplit[j];
						}
						else 
						{
							for(int i=j;i<stopNameSplit.length;i++) 
							{
								stopNameFlagstopSwitched+=stopNameSplit[i]+" ";
							}
							stopNameFlagstopSwitched+=stopNameSplit[j-1];
						}
					}
					else 
					{
						if (stopNameSplit[j-1].length()==2) 
						{
							for(int i=j;i<stopNameSplit.length;i++) 
							{
								stopNameFlagstopSwitched+=stopNameSplit[i]+" ";
							}
							stopNameFlagstopSwitched+=stopNameSplit[j-1];
						}
						else 
						{
							stopNameFlagstopSwitched = stopName;
						}
					}
					//System.out.println(stopNameFlagstopSwitched);
					busStopTST.put(stopNameFlagstopSwitched, stops.get(value));
				}
				value++;
				
				line1 = br1.readLine();
				
			}
			br1.close();
			
			
			FileReader fr2 = new FileReader(transfersFile);
			BufferedReader br2 = new BufferedReader(fr2);
			String line2 = br2.readLine();
			int fromStopID;
			int toStopID;
			int transferType;
			int minTransferTime = -1;
			line2 = br2.readLine();
			while(line2!=null) 
			{
				String[] lineSplit = line2.trim().split(",");
				/*
				for(int i=0;i<lineSplit.length;i++) 
				{
					System.out.println(lineSplit[i]);
				}
				System.out.print("\n");
				*/
				if(!lineSplit[0].contains(" ")) 
				{
					fromStopID = Integer.parseInt(lineSplit[0]);
				}
				else 
				{
					fromStopID = -1;
				}
				if(!lineSplit[1].contains(" "))
				{
					toStopID = Integer.parseInt(lineSplit[1]);
				}
				else 
				{
					toStopID = -1;
				}
				if(!lineSplit[2].contains(" "))
				{
					transferType = Integer.parseInt(lineSplit[2]);
				}
				else 
				{
					transferType = -1;
				}
				
				if(lineSplit.length<4) 
				{
					trips.add(new Trip(fromStopID,toStopID,transferType));
				}
				else 
				{
					if(!lineSplit[3].contains(" ")) 
					{
						minTransferTime = Integer.parseInt(lineSplit[3]);
					}
					else 
					{
						minTransferTime = -1;
					}
					trips.add(new Trip(fromStopID,toStopID,transferType,minTransferTime));
				}
				
				
				line2 = br2.readLine();
			}
			br2.close();
			
			FileReader fr3 = new FileReader(stopTimesFile);
			BufferedReader br3 = new BufferedReader(fr3);
			String line3 = br3.readLine();
			int tripID;
			String arrivalTime;
			String departureTime;
			int stopIDstopTimes;
			int stopSequence;
			String stopHeadsign;
			int pickupType;
			int dropoffType;
			double shapeDistTravelled;
			line3 = br3.readLine();
			int count = 1; //Seperates the different stops with the exact same arrival time 
			while(line3!=null) 
			{
				String[] lineSplit = line3.trim().split(",");
				/*
				for(int i=0;i<lineSplit.length;i++) 
				{
					System.out.println(lineSplit[i]);
				}
				System.out.print("\n");
				*/
				if(!lineSplit[0].equals(" ")) 
				{
					tripID = Integer.parseInt(lineSplit[0]);
				}
				else 
				{
					tripID = -1;
				}
				if(!lineSplit[1].equals(" "))
				{
					arrivalTime = lineSplit[1];
				}
				else 
				{
					arrivalTime = null;
				}
				if(!lineSplit[2].equals(" "))
				{
					departureTime = lineSplit[2];
				}
				else 
				{
					departureTime = null;
				}
				if(!lineSplit[3].equals(" "))
				{
					stopIDstopTimes = Integer.parseInt(lineSplit[3]);
				}
				else 
				{
					stopIDstopTimes = -1;
				}
				if(!lineSplit[4].equals(" "))
				{
					stopSequence = Integer.parseInt(lineSplit[4]);
				}
				else 
				{
					stopSequence = -1;
				}
				if(!lineSplit[5].equals(" "))
				{
					stopHeadsign = lineSplit[5];
				}
				else 
				{
					stopHeadsign = null;
				}
				if(!lineSplit[6].equals(" "))
				{
					pickupType = Integer.parseInt(lineSplit[6]);
				}
				else 
				{
					pickupType = -1;
				}
				if(!lineSplit[7].equals(" "))
				{
					dropoffType = Integer.parseInt(lineSplit[7]);
				}
				else 
				{
					dropoffType = -1;
				}
				
				if(lineSplit.length<9) 
				{
					StopTime newStopTime = new StopTime(tripID,arrivalTime, departureTime, stopIDstopTimes,
							stopSequence, stopHeadsign, pickupType, dropoffType);
					if (newStopTime.isArrivalTimeValid()) 
					{
						stopTimes.add(newStopTime);
						stopTimeTST.put(newStopTime.getArrivalTime() + " "+count, newStopTime);
					}
					else 
					{
						//Do not add
					}
				}
				else 
				{
					if(!lineSplit[8].equals(" "))
					{
						shapeDistTravelled = Double.parseDouble((lineSplit[8]));
					}
					else 
					{
						shapeDistTravelled = -1;
					}
					StopTime newStopTime = new StopTime(tripID,arrivalTime, departureTime, stopIDstopTimes,
							stopSequence, stopHeadsign, pickupType, dropoffType, shapeDistTravelled);
					if (newStopTime.isArrivalTimeValid()) 
					{
						stopTimes.add(newStopTime);
						stopTimeTST.put(newStopTime.getArrivalTime() + " "+count, newStopTime);
					}
					else 
					{
						//Do not add
					}
				}
				count++;
			
				line3 = br3.readLine();
			}

			br3.close();
		}
		catch (Exception e) 
		{
			System.err.println("Error in reading in file");
			stops = null;
			trips = null;
			stopTimes = null;
		}
	}
	
	/*
	 * @param :	stopID1 = source node of shortest path
	 * 			stopID2 = end node of shortest path
	 * @return void: finds the shortest paths between 2 bus stops and prints the distance
	 * Prints the cost and the list of stops in between
	 * If there is no path between them, "No path between these stops" is printed
	 */
	public void shortestPaths(int stopID1, int stopID2) 
	{
		EdgeWeightedDigraph map = new EdgeWeightedDigraph(stops.size());
		for(int i = 0;i<trips.size();i++)
		{
			int fromStopIDIndex = -1;
			int toStopIDIndex = -1;
			int fromStopID = trips.get(i).getFromStopID();
			int toStopID = trips.get(i).getToStopID();
			boolean fromStopIDFound = false;
			boolean toStopIDFound = false;
			
			for (int j = 0;j<stops.size();j++) 
			{
				if (stops.get(j).getStopID()==fromStopID) 
				{
					fromStopIDIndex = j;
					fromStopIDFound = true;
				}
				if(stops.get(j).getStopID()==toStopID) 
				{
					toStopIDIndex = j;
					toStopIDFound = true;
				}
				if(fromStopIDFound&&toStopIDFound) 
				{
					break;
				}
			}
			
			int transferType = trips.get(i).getTransferType();
			double transferTime = -1.0;
			if(transferType==0) 
			{
				transferTime = 2;
			}
			else if (transferType==2) 
			{
				transferTime = (trips.get(i).getMinTransferTime())/100;
			}
			map.addEdge(new DirectedEdge(fromStopIDIndex,toStopIDIndex,transferTime));
			
		}
		for(int i = 0;i<stopTimes.size()-1;i++) 
		{
			
			int prevTripID = stopTimes.get(i).getTripID();
			int currentTripID = stopTimes.get(i+1).getTripID();
			if (prevTripID==currentTripID) 
			{
				int prevStopIDIndex = -1;
				int currentStopIDIndex = -1;
				int prevStopID = stopTimes.get(i).getStopID();
				int currentStopID = stopTimes.get(i+1).getStopID();
				boolean prevStopIDFound = false;
				boolean currentStopIDFound = false;
				for (int j = 0;j<stops.size();j++) 
				{
					if (stops.get(j).getStopID()==prevStopID) 
					{
						prevStopIDIndex = j;
						prevStopIDFound = true;
					}
					if(stops.get(j).getStopID()==currentStopID) 
					{
						currentStopIDIndex = j;
						currentStopIDFound = true;
					}
					if(prevStopIDFound&&currentStopIDFound) 
					{
						break;
					}
				}
				map.addEdge(new DirectedEdge(prevStopIDIndex,currentStopIDIndex,1.0));
			}
			else 
			{
				//Move on to next trip ID
			}
			
		}
		int stopID1Index = -1;
		int stopID2Index = -1;
		boolean stopID1IndexFound = false;
		boolean stopID2IndexFound = false;
		for (int i = 0;i<stops.size();i++) 
		{
			if (stops.get(i).getStopID()==stopID1) 
			{
				stopID1Index = i;
				stopID1IndexFound = true;
			}
			if(stops.get(i).getStopID()==stopID2) 
			{
				stopID2Index = i;
				stopID2IndexFound = true;
			}
			if(stopID1IndexFound&&stopID2IndexFound) 
			{
				break;
			}
		}
		if (stopID1IndexFound&&stopID2IndexFound) 
		{
			DijkstraSP sp = new DijkstraSP(map,stopID1Index);
			if (sp.hasPathTo(stopID2Index)) 
			{
				System.out.println("Shortest path between "+stopID1+" and "+stopID2+": "+sp.distTo(stopID2Index));
				System.out.println("List of stops (in brackets, cost of each path)");
				printShortestPathList(sp,stopID1Index,stopID2Index);
			}
			else 
			{
				System.err.print("No path between these stops");
			}
		}
		else 
		{
			if (stopID2IndexFound&&!stopID1IndexFound) 
			{
				System.err.print("Stop ID "+stopID1+" cannot be found");
			}
			else if(stopID1IndexFound&&!stopID2IndexFound) 
			{
				System.err.print("Stop ID "+stopID2+" cannot be found");
			}
			else 
			{
				System.err.print("Stop IDs "+stopID1+" and "+stopID2+" cannot be found");
			}
		}
	}
	
	/*
	 * @param: 	sp = Dijkstra shortest paths from stop ID 1 inputted by user
	 * 			startStopID = the index of the vertex the shortest path is coming from
	 * 			endStopID = the index of the vertex the shortest path is going to
	 * @return void: prints the stops gone through on the path from one stop to another
	 */
	public void printShortestPathList(DijkstraSP sp, int startStopID, int endStopID) 
	{
		ArrayList<DirectedEdge> shortestPathList = new ArrayList<DirectedEdge>();
		boolean endOfList = false;
		DirectedEdge prevEdge = sp.edgeTo(endStopID);
		shortestPathList.add(prevEdge);
		while (!endOfList) 
		{
			DirectedEdge currentEdge = sp.edgeTo(prevEdge.from());
			shortestPathList.add(currentEdge);
			if (currentEdge.from()==startStopID) 
			{
				endOfList = true;
			}
			prevEdge = currentEdge;
			
		}
		for (int i = shortestPathList.size()-1;i>=0;i--) 
		{
			if (i == shortestPathList.size()-1) 
			{
				System.out.print(stops.get(shortestPathList.get(i).from()).getStopID() +"->");
			}
			else 
			{
				System.out.print(stops.get(shortestPathList.get(i).from()).getStopID() +" ("+shortestPathList.get(i).weight() +")->");
			}
		}
		System.out.println(stops.get(endStopID).getStopID());
	}
	
	/*
	 * @param String busStopName: the (partial) name of the stop inputted by user
	 * @return busStop: searches for bus stops by full name or by the first few characters in the name
	 */
	public BusStop[] busStopSearch (String busStopName) 
	{
		Queue<String> busStopsFound;
		busStopsFound = (Queue<String>) busStopTST.keysWithPrefix(busStopName);
		BusStop[] busStopSearch = new BusStop[busStopsFound.size()];
		if(busStopsFound.size()>0) 
		{
			for (int i = 0;i<busStopsFound.size();i++) 
			{
				System.out.println("Stop "+(i+1));
				BusStop newStop = busStopTST.get(busStopsFound.dequeue());
				busStopSearch[i] = newStop;
				System.out.println("Stop Name: "+newStop.getStopName()+"\nStop ID: "+newStop.getStopID()
					+"\nStop Description: "+newStop.getStopDesc()+"\nStop Longitude: "+newStop.getStopLon()
					+"\nStop Latitude: "+newStop.getStopLat()+"\nLocation Type: "+newStop.getLocationType()
					+"\nParent Station: "+newStop.getParentStation()+"\nStop Code: "+newStop.getStopCode()
					+"\nStop URL: "+newStop.getStopURL()+"\nZone ID: "+newStop.getZoneID()+"\n");
				
			}
		}
		else 
		{
			System.out.println("No stops found");
		}
		return busStopSearch;
	}
	
	/*
	 * @param String arrivalTime: the arrival time to be searched for inputted by user
	 * @return Trip[]: finds all stops with a given arrival time sorted by trip id
	 */
	public StopTime[] tripArrivalTimeSearch (String arrivalTime) 
	{
		
		Queue<String> arrivalTimesFound = (Queue<String>) stopTimeTST.keysWithPrefix(arrivalTime);
		StopTime[] arrivalTimeSearch = new StopTime[arrivalTimesFound.size()];
		
		if(arrivalTimesFound.size()>0) 
		{
			for (int i = 0;i<arrivalTimeSearch.length;i++) 
			{
				StopTime newStop = stopTimeTST.get(arrivalTimesFound.dequeue());
				arrivalTimeSearch[i] = newStop;
				//Insertion sort
				
				boolean tripIDSmaller = true;
				int j = i;
				while(tripIDSmaller) 
				{
					if (j==0) 
					{
						
						tripIDSmaller=false;
					}
					
					else if ((arrivalTimeSearch[j-1].getTripID()<newStop.getTripID())) 
					{
						tripIDSmaller=false;
						
					}
					else 
					{
						arrivalTimeSearch[j]=arrivalTimeSearch[j-1];
						arrivalTimeSearch[j-1]=newStop;
						j--;
					}
				}
				
				
			}
			//Prints out the stop time data
			for(int i=0;i<arrivalTimeSearch.length;i++) 
			{
				
				System.out.println("Stop Time "+(i+1));
				System.out.println("Trip ID: "+arrivalTimeSearch[i].getTripID()+"\nArrival Time: "+arrivalTimeSearch[i].getArrivalTime()
				+"\nDeparture Time: "+arrivalTimeSearch[i].getDepartureTime()+"\nStop ID: "+arrivalTimeSearch[i].getStopID()
				+"\nStop Sequence: "+arrivalTimeSearch[i].getStopSequence()+"\nStop Headsign: "+arrivalTimeSearch[i].getStopHeadsign()
				+"\nPickup Type: "+arrivalTimeSearch[i].getPickupType()+"\nDropoff Type: "+arrivalTimeSearch[i].getDropoffType()
				+"\nShape Dist Travelled: "+arrivalTimeSearch[i].getShapeDistTravelled()+"\n");
				
			}
			
		}
		else 
		{
			System.out.println("No stop time found");
		}
		 
		 
		return arrivalTimeSearch;
		//TODO
		 
		
	}
	
	/*
	 * Main: frontend feature to enable use of the above methods
	 */
	public static void main (String[] args)
	{
		BusSystem system = new BusSystem("/Users/matthanrahan321/eclipse-workspace/finalProject/finalProject/stops.txt",
				"/Users/matthanrahan321/eclipse-workspace/finalProject/finalProject/transfers.txt",
				"/Users/matthanrahan321/eclipse-workspace/finalProject/finalProject/stop_times.txt");
		Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to the Vancouver Bus System");
		boolean exit = false;
		do 
		{
			System.out.println("Type 'continue' to continue or 'exit' to leave the program: ");
			String input = scanner.next().trim();
			if (input.equalsIgnoreCase("continue"))
			{
				System.out.println("Type 'a' to find the shortest paths between 2 bus stops\n"
						+ "Type 'b' to search for bus stops by full name/first characters in name\n"
						+ "Type 'c' to search for all trips with a given arrival time\n"
						+ "Type 'Exit' to exit the program\n");
				input = scanner.next().trim();
				if(input.equalsIgnoreCase("a")) 
				{
					int stopIDchosen1;
					int stopIDchosen2;
					boolean isValidStopID = true;
					do 
					{
						System.out.println("Enter bus stop ID 1: ");
						if(scanner.hasNextInt())
						{
							stopIDchosen1 = scanner.nextInt();
							isValidStopID = true;
							do 
							{								
								System.out.println("Enter bus stop ID 2: ");
								if(scanner.hasNextInt()) 
								{
									stopIDchosen2 = scanner.nextInt();
									isValidStopID = true;
									System.out.println("Calculating...");
									system.shortestPaths(stopIDchosen1,stopIDchosen2);
									
								}
								else
								{
									//TODO - accept full name of stop
									isValidStopID = false;
									scanner.next();
									System.err.println("Error: input a valid number");
								}
							}while(!isValidStopID);
						}
						else 
						{
							isValidStopID = false;
							scanner.next();
							System.err.println("Error: input a valid number");
						}
					}while(!isValidStopID);
				}
				else if(input.equalsIgnoreCase("b")) 
				{
					Scanner searchScanner = new Scanner(System.in);
					System.out.println("Enter the bus stop name/first few characters of name: ");
					boolean inputIsBlank = true;
					while (inputIsBlank) 
					{
						input = searchScanner.nextLine().trim();
						if (!input.equals("")) 
						{
							System.out.println("Searching...");
							system.busStopSearch(input.toUpperCase());
							inputIsBlank = false;
						}
					}
				}
				else if(input.equalsIgnoreCase("c")) 
				{
					Scanner searchScanner = new Scanner(System.in);
					boolean inputCorrect = true;
					do 
					{
						System.out.println("Enter arrival time (hh:mm:ss): ");
						input = searchScanner.next().trim();
						inputCorrect = true;
						String[] checkInput = input.split(":");
						try 
						{
							if((checkInput[0].length()==2)&&(checkInput[1].length()==2)&&(checkInput[2].length()==2)
									&&(Integer.parseInt(checkInput[0])<24)&&(Integer.parseInt(checkInput[1])<60)
									&&(Integer.parseInt(checkInput[2])<60))
							{
								System.out.println("Searching...");
								system.tripArrivalTimeSearch(input);
							}
							else 
							{
								inputCorrect = false;
								System.err.println("Error: input a valid time");
							}
						}
						catch (Exception e) 
						{
							inputCorrect = false;
							System.err.println("Error: input a valid time");
						}
						
					}while(!inputCorrect);
				}
				else if(input.equalsIgnoreCase("Exit")) 
				{
					exit = true;
				}
				else 
				{
					System.err.println("Error: enter one of the options above");
				}
			}
			else if (input.equalsIgnoreCase("exit"))
			{
				exit = true;
			}
			else 
			{
				System.err.println("Error: enter one of the options above");
			}
			System.out.print("\n");
		}while(!exit);
		System.out.println("Goodbye!");
		scanner.close();
	}
}
