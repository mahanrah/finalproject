package finalProject;
//trip_id,arrival_time,departure_time,stop_id,stop_sequence,stop_headsign,pickup_type,drop_off_type,shape_dist_traveled

public class StopTime {
	private int tripID;
	private int arrivalHour;
	private int arrivalMinute;
	private int arrivalSecond;
	private int departureHour;
	private int departureMinute;
	private int departureSecond;
	private int stopID;
	private int stopSequence;
	private String stopHeadsign;
	private int pickupType;
	private int dropoffType;
	private double shapeDistTravelled;
	
	StopTime( int tripID, String arrivalTime, String departureTime, int stopID, int stopSequence,
			 String stopHeadsign, int pickupType, int dropoffType, double shapeDistTravelled)
	{
		this.setTripID(tripID);
		this.setStopID(stopID);
		this.setStopSequence(stopSequence);
		this.setStopHeadsign(stopHeadsign);
		this.setPickupType(pickupType);
		this.setDropoffType(dropoffType);
		this.setShapeDistTravelled(shapeDistTravelled);
		String[] timeSplit = arrivalTime.trim().split(":");
		if(Integer.parseInt(timeSplit[0])<24) 
		{
			setArrivalHour(Integer.parseInt(timeSplit[0]));
		}
		else 
		{
			setArrivalHour(-1);
		}
		if(Integer.parseInt(timeSplit[1])<60) 
		{
			setArrivalMinute(Integer.parseInt(timeSplit[1]));
		}
		else 
		{
			setArrivalMinute(-1);
		}
		if(Integer.parseInt(timeSplit[2])<60) 
		{			
			setArrivalSecond(Integer.parseInt(timeSplit[2]));
		}
		else 
		{
			setArrivalSecond(-1);
		}
		timeSplit = departureTime.trim().split(":");
		if(Integer.parseInt(timeSplit[0])<24) 
		{
			setDepartureHour(Integer.parseInt(timeSplit[0]));
		}
		else 
		{
			setDepartureHour(-1);
		}
		if(Integer.parseInt(timeSplit[1])<60) 
		{
			setDepartureMinute(Integer.parseInt(timeSplit[1]));
		}
		else 
		{
			setDepartureMinute(-1);
		}
		if(Integer.parseInt(timeSplit[2])<60) 
		{			
			setDepartureSecond(Integer.parseInt(timeSplit[2]));
		}
		else 
		{
			setDepartureSecond(-1);
		}
	}
	
	StopTime( int tripID, String arrivalTime, String departureTime, int stopID, int stopSequence,
			 String stopHeadsign, int pickupType, int dropoffType)
	{
		this.setTripID(tripID);
		this.setStopID(stopID);
		this.setStopSequence(stopSequence);
		this.setStopHeadsign(stopHeadsign);
		this.setPickupType(pickupType);
		this.setDropoffType(dropoffType);
		String[] timeSplit = arrivalTime.trim().split(":");
		if(Integer.parseInt(timeSplit[0])<24) 
		{
			setArrivalHour(Integer.parseInt(timeSplit[0]));
		}
		else 
		{
			setArrivalHour(-1);
		}
		if(Integer.parseInt(timeSplit[1])<60) 
		{
			setArrivalMinute(Integer.parseInt(timeSplit[1]));
		}
		else 
		{
			setArrivalMinute(-1);
		}
		if(Integer.parseInt(timeSplit[2])<60) 
		{			
			setArrivalSecond(Integer.parseInt(timeSplit[2]));
		}
		else 
		{
			setArrivalSecond(-1);
		}
		timeSplit = departureTime.trim().split(":");
		if(Integer.parseInt(timeSplit[0])<24) 
		{
			setDepartureHour(Integer.parseInt(timeSplit[0]));
		}
		else 
		{
			setDepartureHour(-1);
		}
		if(Integer.parseInt(timeSplit[1])<60) 
		{
			setDepartureMinute(Integer.parseInt(timeSplit[1]));
		}
		else 
		{
			setDepartureMinute(-1);
		}
		if(Integer.parseInt(timeSplit[2])<60) 
		{			
			setDepartureSecond(Integer.parseInt(timeSplit[2]));
		}
		else 
		{
			setDepartureSecond(-1);
		}
	}

	public int getTripID() {
		return tripID;
	}

	public void setTripID(int tripID) {
		this.tripID = tripID;
	}

	public int getStopID() {
		return stopID;
	}

	public void setStopID(int stopID) {
		this.stopID = stopID;
	}

	public int getArrivalHour() {
		return arrivalHour;
	}

	public void setArrivalHour(int arrivalHour) {
		this.arrivalHour = arrivalHour;
	}

	public int getArrivalMinute() {
		return arrivalMinute;
	}

	public void setArrivalMinute(int arrivalMinute) {
		this.arrivalMinute = arrivalMinute;
	}

	public int getArrivalSecond() {
		return arrivalSecond;
	}

	public void setArrivalSecond(int arrivalSecond) {
		this.arrivalSecond = arrivalSecond;
	}

	public int getDepartureHour() {
		return departureHour;
	}

	public void setDepartureHour(int departureHour) {
		this.departureHour = departureHour;
	}

	public int getDepartureMinute() {
		return departureMinute;
	}

	public void setDepartureMinute(int departureMinute) {
		this.departureMinute = departureMinute;
	}

	public int getDepartureSecond() {
		return departureSecond;
	}

	public void setDepartureSecond(int departureSecond) {
		this.departureSecond = departureSecond;
	}

	public int getStopSequence() {
		return stopSequence;
	}

	public void setStopSequence(int stopSequence) {
		this.stopSequence = stopSequence;
	}

	public String getStopHeadsign() {
		return stopHeadsign;
	}

	public void setStopHeadsign(String stopHeadsign) {
		this.stopHeadsign = stopHeadsign;
	}

	public int getPickupType() {
		return pickupType;
	}

	public void setPickupType(int pickupType) {
		this.pickupType = pickupType;
	}

	public int getDropoffType() {
		return dropoffType;
	}

	public void setDropoffType(int dropoffType) {
		this.dropoffType = dropoffType;
	}

	public double getShapeDistTravelled() {
		return shapeDistTravelled;
	}

	public void setShapeDistTravelled(double shapeDistTravelled) {
		this.shapeDistTravelled = shapeDistTravelled;
	}
	
	public boolean isArrivalTimeValid() 
	{
		if ((arrivalHour>=0)&&(arrivalHour<24)&&(arrivalMinute>=0)&&(arrivalMinute<60)
				&&(arrivalSecond>=0)&&(arrivalSecond<60)) 
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	public String getArrivalTime() 
	{
		String arrivalHourString = "";
		if(getArrivalHour()==0) 
		{
			arrivalHourString = "00";
		}
		else 
		{
			arrivalHourString += getArrivalHour();
		}
		String arrivalMinuteString = "";
		if(getArrivalMinute()==0) 
		{
			arrivalMinuteString = "00";
		}
		else 
		{
			arrivalMinuteString += getArrivalMinute();
		}
		String arrivalSecondString = "";
		if(getArrivalSecond()==0) 
		{
			arrivalSecondString = "00";
		}
		else 
		{
			arrivalSecondString += getArrivalSecond();
		}
		String returnString = arrivalHourString+":"+arrivalMinuteString+":"+arrivalSecondString;
		return returnString;
	}
	public String getDepartureTime() 
	{
		String departureHourString = "";
		if(getDepartureHour()==0) 
		{
			departureHourString = "00";
		}
		else 
		{
			departureHourString += getDepartureHour();
		}
		String departureMinuteString = "";
		if(getDepartureMinute()==0) 
		{
			departureMinuteString = "00";
		}
		else 
		{
			departureMinuteString += getDepartureMinute();
		}
		String departureSecondString = "";
		if(getDepartureSecond()==0) 
		{
			departureSecondString = "00";
		}
		else 
		{
			departureSecondString += getDepartureSecond();
		}
		String returnString = departureHourString+":"+departureMinuteString+":"+departureSecondString;
		return returnString;
	}
}
