package finalProject;

public class Trip {
	private int fromStopID;
	private int toStopID;
	private int transferType;
	private int minTransferTime;
	
	Trip(int fromStopID, int toStopID, int transferType, int minTransferTime)
	{
		this.setFromStopID(fromStopID);
		this.setToStopID(toStopID);
		this.setTransferType(transferType);
		this.setMinTransferTime(minTransferTime);
	}
	
	Trip(int fromStopID, int toStopID, int transferType)
	{
		this.setFromStopID(fromStopID);
		this.setToStopID(toStopID);
		this.setTransferType(transferType);
	}

	public int getFromStopID() {
		return fromStopID;
	}

	public void setFromStopID(int fromStopID) {
		this.fromStopID = fromStopID;
	}

	public int getToStopID() {
		return toStopID;
	}

	public void setToStopID(int toStopID) {
		this.toStopID = toStopID;
	}

	public int getTransferType() {
		return transferType;
	}

	public void setTransferType(int transferType) {
		this.transferType = transferType;
	}

	public int getMinTransferTime() {
		return minTransferTime;
	}

	public void setMinTransferTime(int minTransferTime) {
		this.minTransferTime = minTransferTime;
	}
}
