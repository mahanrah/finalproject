package finalProject;

public class BusStop {

	private int stopID;
	private int stopCode;
	private String stopName;
	private String stopDesc;
	private Double stopLat;
	private Double stopLon;
	private String zoneID;
	private String stopURL;
	private int locationType;
	private int parentStation;
	
	BusStop( int stopID, int stopCode, String stopName, String stopDesc, Double stopLat, Double stopLon, 
			String zoneID, String stopURL, int locationType, int parentStation)
	{
		this.setStopID(stopID);
		this.setStopCode(stopCode);
		this.setStopName(stopName);
		this.setStopDesc(stopDesc);
		this.setStopLat(stopLat);
		this.setStopLon(stopLon);
		this.setZoneID(zoneID);
		this.setStopURL(stopURL);
		this.setLocationType(locationType);
		this.setParentStation(parentStation);
	}
	
	BusStop( int stopID, int stopCode, String stopName, String stopDesc, Double stopLat, Double stopLon, 
			String zoneID, String stopURL, int locationType)
	{
		this.setStopID(stopID);
		this.setStopCode(stopCode);
		this.setStopName(stopName);
		this.setStopDesc(stopDesc);
		this.setStopLat(stopLat);
		this.setStopLon(stopLon);
		this.setZoneID(zoneID);
		this.setStopURL(stopURL);
		this.setLocationType(locationType);
	}

	public int getStopID() {
		return stopID;
	}

	public void setStopID(int stopID) {
		this.stopID = stopID;
	}

	public int getStopCode() {
		return stopCode;
	}

	public void setStopCode(int stopCode) {
		this.stopCode = stopCode;
	}

	public String getStopName() {
		return stopName;
	}

	public void setStopName(String stopName) {
		this.stopName = stopName;
	}

	public String getStopDesc() {
		return stopDesc;
	}

	public void setStopDesc(String stopDesc) {
		this.stopDesc = stopDesc;
	}

	public Double getStopLat() {
		return stopLat;
	}

	public void setStopLat(Double stopLat) {
		this.stopLat = stopLat;
	}

	public Double getStopLon() {
		return stopLon;
	}

	public void setStopLon(Double stopLon) {
		this.stopLon = stopLon;
	}

	public String getZoneID() {
		return zoneID;
	}

	public void setZoneID(String zoneID) {
		this.zoneID = zoneID;
	}

	public String getStopURL() {
		return stopURL;
	}

	public void setStopURL(String stopURL) {
		this.stopURL = stopURL;
	}

	public int getLocationType() {
		return locationType;
	}

	public void setLocationType(int locationType) {
		this.locationType = locationType;
	}

	public int getParentStation() {
		return parentStation;
	}

	public void setParentStation(int parentStation) {
		this.parentStation = parentStation;
	}
}
